@extends('imageLibrary::layouts.app')

@section('content')

    <?php /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary[] $imageLibraries */ ?>

    {!! html()->a(route('image-libraries.create'), 'Add Image Library') !!}<br>
    {!! html()->a(route('image-libraries.conversions.index'), 'Conversions') !!}<br>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Title</th>
            <th>Options</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>

        @forelse($imageLibraries as $imageLibrary)
            <tr>
                <td>{{ $imageLibrary->id }}</td>
                <td>{{ html()->img($imageLibrary->getFirstMediaUrl())->attribute('width', 80) }}</td>
                <td>{{ $imageLibrary->title }}</td>
                <td>
                    {{ html()->a(route('image-libraries.show', $imageLibrary), 'Show') }}
                    {{ html()->a(route('image-libraries.edit', $imageLibrary), 'Edit') }}
                </td>
                <td>{{ $imageLibrary->created_at }}</td>
            </tr>

        @empty
            <p>no image library</p>
        @endforelse

        </tbody>
    </table>
    {!! $imageLibraries->links() !!}

@endsection
