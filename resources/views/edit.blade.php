@extends('imageLibrary::layouts.app')

@section('content')

    <?php /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary $imageLibrary */ ?>

    {{ html()->modelForm($imageLibrary, 'patch', route('image-libraries.update', $imageLibrary))->acceptsFiles()->open() }}

    {{ html()->label('Title', 'title') }}
    {{ html()->text('title')->placeholder('Enter title') }}<br>

    {{ html()->img($imageLibrary->getFirstMediaUrl())->attribute('width', 250) }}<br>
    {{ html()->file('image') }}<br>

    {{ html()->label('Conversions:') }}

    {{
        html()
            ->multiselect(
                'conversions[]',
                ImageLibrary::repositoryConversion()->pluck('name', 'id'),
                $imageLibrary->conversions->pluck('id')->toArray()
            )
            ->placeholder('Select Conversions')
     }}

    {{ html()->submit("Upload images for '{$imageLibrary->title}'") }}

    {{ html()->form()->close() }}

@endsection
