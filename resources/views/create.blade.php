@extends('imageLibrary::layouts.app')

@section('content')

    {{ html()->form('post', route('image-libraries.store'))->acceptsFiles()->open() }}

    {{ html()->label('Title', 'title') }}
    {{ html()->text('title')->placeholder('Enter Title') }}<br>

        {{
            html()->file('image')
        }}

    {{ html()->button('Create Image Library') }}
    {{ html()->form()->close() }}

@endsection
