@extends('imageLibrary::layouts.app')

@section('content')

    <?php /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary $imageLibrary */ ?>

    <table class="table">
        <thead>
        <tr>
            <th>Conversions</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>

        @forelse($imageLibrary->conversions as $conversion)

                @php
                    $media = $imageLibrary->getFirstMedia();
                @endphp

                <tr>
                    <td> {{
                            html()->a(
                                route('image-libraries.conversions.edit', $conversion),
                                $conversion->name
                                )
                                ->attribute('title', "Click to edit '{$conversion->name}' conversion")
                        }}
                    </td>
                    <td>
                        {!! html()->a(route('image-libraries.edit', $imageLibrary), 'Edit/Upload image') !!}
                        {!! html()->a($media->getUrl($conversion->name), 'Show Converted')->attribute('target', '_blank') !!}
                    </td>
                </tr>
            @empty
                <p>no conversion</p>
            @endforelse

        </tbody>
    </table>

@endsection
