@extends('imageLibrary::layouts.app')

@section('content')

    <?php /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion $conversion */ ?>

    {{ html()->modelForm($conversion, 'PATCH', route('image-libraries.conversions.update', $conversion))->open() }}

    <p>warning: renaming conversion name will take effect on your calling images</p>
    {{ html()->text('name')->placeholder('Enter Name') }}<br>
    {{ html()->select('fit', config('image-library.fit'))->placeholder('Select Fit') }}<br>
    {{ html()->select('format', config('image-library.format'))->placeholder('Select Format') }}<br>
    {{ html()->number('width')->placeholder('Enter Width') }}<br>
    {{ html()->number('height')->placeholder('Enter height') }}
    <p>Optimize{{ html()->checkbox('is_optimize') }}</p>

    {{ html()->button('Update Conversion') }}

    {{ html()->form()->close() }}


@endsection
