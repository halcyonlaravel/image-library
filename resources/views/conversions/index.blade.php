@extends('imageLibrary::layouts.app')

@section('content')

    <?php /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion[] $conversions */ ?>

    {{ html()->form()->open() }}

    {{ html()->text('name')->placeholder('Enter Name') }}<br>
    {{ html()->select('fit', config('image-library.fit'))->placeholder('Select Fit') }}<br>
    {{ html()->select('format', config('image-library.format'))->placeholder('Select Format') }}<br>
    {{ html()->number('width')->placeholder('Enter Width') }}<br>
    {{ html()->number('height')->placeholder('Enter height') }}
    <p>Optimize{{ html()->checkbox('is_optimize', true) }}</p>

    {{ html()->button('Add Conversion') }}

    {{ html()->form()->close() }}

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Fit</th>
            <th>Format</th>
            <th>Width</th>
            <th>Height</th>
            <th>Optimized</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>

        @forelse($conversions as $conversion)
            <tr>
                <td>{{ $conversion->id }}</td>
                <td>{{ $conversion->name }}</td>
                <td>{{ $conversion->fit }}</td>
                <td>{{ $conversion->format }}</td>
                <td>{{ $conversion->width }}</td>
                <td>{{ $conversion->height }}</td>
                <td>{{ $conversion->is_optimize?'Yes':'No' }}</td>
                <td>{{ html()->a(route('image-libraries.conversions.edit', $conversion), 'Edit') }}</td>
            </tr>

        @empty
            <p>no image conversion</p>
        @endforelse

        </tbody>
    </table>


@endsection
