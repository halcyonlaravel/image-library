<?php

use Spatie\Image\Manipulations;

return [
    'fit' => [
        Manipulations::FIT_CONTAIN => ucfirst(Manipulations::FIT_CONTAIN),
        Manipulations::FIT_MAX => ucfirst(Manipulations::FIT_MAX),
        Manipulations::FIT_FILL => ucfirst(Manipulations::FIT_FILL),
        Manipulations::FIT_STRETCH => ucfirst(Manipulations::FIT_STRETCH),
        Manipulations::FIT_CROP => ucfirst(Manipulations::FIT_CROP),
    ],
    'format' => [
        Manipulations::FORMAT_JPG => ucfirst(Manipulations::FORMAT_JPG),
        Manipulations::FORMAT_PJPG => ucfirst(Manipulations::FORMAT_PJPG),
        Manipulations::FORMAT_PNG => ucfirst(Manipulations::FORMAT_PNG),
        Manipulations::FORMAT_GIF => ucfirst(Manipulations::FORMAT_GIF),
        Manipulations::FORMAT_WEBP => ucfirst(Manipulations::FORMAT_WEBP),
    ],
    'tables' => [
        // models
        'image_libraries' => 'image_libraries_image_libraries',
        'conversions' => 'image_libraries_conversions',

        // polymorphic
        'imageables' => 'image_libraries_imageables',

        // pivots
        'conversion_image_libraries' => 'image_libraries_conversion_image_libraries',
    ],
];
