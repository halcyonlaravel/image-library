# Halcyon Laravel Boilerplate Image Library #

### Changes Log ###
    2019-11-26 v1.0.2: Add access repo conversion using facade
    2019-11-26 v1.0.0: First Stable Release
    2019-11-25 v0.1.1: Typo migration name
    2019-11-25 v0.1.0: Refactor to a new idea
    2019-11-22 v0.0.10: Fix
    2019-11-22 v0.0.9: Add on interface
    2019-11-22 v0.0.8: Add validation
    2019-11-22 v0.0.7: Fix
    2019-11-22 v0.0.6: Fix migration location (typo)
    2019-11-22 v0.0.5: Fix migration location
    2019-11-22 v0.0.4: Add migration file
    2019-11-22 v0.0.3: Manage blade layout
    2019-11-22 v0.0.2: Fix None need, ide helper
    2019-11-22 v0.0.1: Initial pre release