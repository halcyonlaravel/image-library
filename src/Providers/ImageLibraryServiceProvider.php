<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Providers;

use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepositoryEloquent;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepositoryEloquent;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class ImageLibraryServiceProvider extends ServiceProvider
{
    public function boot(Filesystem $filesystem)
    {
        $this->publishes([
            __DIR__.'/../../config/image-library.php' => config_path('image-library.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/imageLibrary'),
        ], 'views');

        $this->publishes([
            __DIR__.'/../../database/migrations/create_image_library_tables.php.stub' => $this->getMigrationFileName($filesystem),
        ], 'migrations');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'imageLibrary');

        $this->app->bind(ImageLibraryRepository::class, ImageLibraryRepositoryEloquent::class);
        $this->app->bind(ConversionRepository::class, ConversionRepositoryEloquent::class);
    }

    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*create_image_library_tables.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}_create_image_library_tables.php")
            ->first();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/image-library.php', 'image-library');

        $this->registerRouteMacro();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function registerRouteMacro()
    {
        $router = $this->app->make('router');

        $router->macro('imageLibrary', function () use ($router) {
            $router->group([
                'prefix' => 'image-libraries',
                'as' => 'image-libraries.',
                'namespace' => '\HalcyonLaravelBoilerplate\ImageLibrary\Http\Controllers',
            ], function () use ($router) {

//                $router->group([
//                    'prefix' => 'collections',
//                    'as' => 'collections.',
//                ], function () use ($router) {
//                    $router->get('/', 'CollectionsController@index')->name('index');
//                    $router->post('/', 'CollectionsController@store')->name('store');
//                    $router->get('/{collection}/edit', 'CollectionsController@edit')->name('edit');
//                    $router->match(['put', 'patch'], '/{collection}', 'CollectionsController@update')->name('update');
//                });

                $router->group([
                    'prefix' => 'conversions',
                    'as' => 'conversions.',
                ], function () use ($router) {
                    $router->get('/', 'ConversionsController@index')->name('index');
                    $router->post('/', 'ConversionsController@store')->name('store');
                    $router->get('/{conversion}/edit', 'ConversionsController@edit')->name('edit');
                    $router->match(['put', 'patch'], '/{conversion}', 'ConversionsController@update')->name('update');
                });

                $router->get('/', 'ImageLibrariesController@index')->name('index');
                $router->get('/create', 'ImageLibrariesController@create')->name('create');
                $router->post('/', 'ImageLibrariesController@store')->name('store');
                $router->get('/{imageLibrary}', 'ImageLibrariesController@show')->name('show');
                $router->get('/{imageLibrary}/edit', 'ImageLibrariesController@edit')->name('edit');
                $router->match(['put', 'patch'], '/{imageLibrary}', 'ImageLibrariesController@update')->name('update');

            });
        });
    }
}
