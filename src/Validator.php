<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary;

use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\InvalidArgumentOnSortingException;
use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\InvalidArgumentOnUploadException;
use Illuminate\Http\UploadedFile;

class Validator
{
    /**
     * @param  array  $idsOrFileUploads
     * @param  bool  $idOnly
     */
    public static function imageLibraryIDsOrFileUploaded(array $idsOrFileUploads, bool $idOnly = false)
    {
        array_map(function ($idsOrFileUploaded) use ($idOnly) {

            if ($idOnly) {
                if (is_int($idsOrFileUploaded) && !blank(Models\ImageLibrary::whereId($idsOrFileUploaded)->first())) {
                    return true;
                } else {
                    throw InvalidArgumentOnSortingException::create();
                }
            } else {
                if ($idsOrFileUploaded instanceof UploadedFile) {
                    return true;
                } elseif (is_int($idsOrFileUploaded) && !blank(Models\ImageLibrary::whereId($idsOrFileUploaded)->first())) {
                    return true;
                } else {
                    throw InvalidArgumentOnUploadException::create();
                }
            }

        }, $idsOrFileUploads);
    }
}