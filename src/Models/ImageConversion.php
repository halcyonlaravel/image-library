<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion
 *
 * @property int $id
 * @property string $name
 * @property string|null $fit use in fit manipulation
 * @property string|null $format use in format manipulation
 * @property int|null $width
 * @property int|null $height
 * @property int $is_optimize
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary $imageLibrary
 * @method static Builder|ImageConversion newModelQuery()
 * @method static Builder|ImageConversion newQuery()
 * @method static Builder|ImageConversion query()
 * @method static Builder|ImageConversion whereCreatedAt($value)
 * @method static Builder|ImageConversion whereFit($value)
 * @method static Builder|ImageConversion whereFormat($value)
 * @method static Builder|ImageConversion whereHeight($value)
 * @method static Builder|ImageConversion whereId($value)
 * @method static Builder|ImageConversion whereIsOptimize($value)
 * @method static Builder|ImageConversion whereName($value)
 * @method static Builder|ImageConversion whereUpdatedAt($value)
 * @method static Builder|ImageConversion whereWidth($value)
 * @mixin \Eloquent
 */
class ImageConversion extends Model
{
    protected $fillable = [
        'name',
        'fit',
        'format',
        'width',
        'height',
        'is_optimize',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('image-library.tables.conversions'));
    }

    public function imageLibrary()
    {
        return $this->belongsTo(ImageLibrary::class);
    }
}
