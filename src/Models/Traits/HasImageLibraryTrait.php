<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Models\Traits;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;

trait HasImageLibraryTrait
{
    /**
     * @var \Spatie\MediaLibrary\Conversion\Conversion[]
     */
    public $imageConversions = [];

    public function imageLibraries()
    {
        return $this->morphToMany(ImageLibrary::class, config('image-library.tables.imageables'));
    }

    /**
     * @param  string  $category
     * @param  string  $conversionName
     *
     * @return string|null
     */
    public function getFirstImageUrl(string $category = 'default', string $conversionName = ''): ?string
    {
        /** @var ImageLibrary $image */
        $image = $this->imageLibraries()->where('category_name', $category)->first();
        return blank($image)
            ? null
            : $image->getFirstMediaUrl('default', $conversionName);
    }

    /**
     * @return int[]
     */
    public function getImageLibraryIds(): array
    {
        $ids = [];
        foreach ($this->imageLibraries as $imageLibrary) {
            $ids[] = $imageLibrary->id;
        }
        return $ids;
    }
}
