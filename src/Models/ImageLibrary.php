<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion[]
 *     $conversions
 * @property-read int|null $conversions_count
 * @property-read \Illuminate\Database\Eloquent\Collection $media
 * @property-read int|null $media_count
 * @method static Builder|ImageLibrary newModelQuery()
 * @method static Builder|ImageLibrary newQuery()
 * @method static Builder|ImageLibrary query()
 * @method static Builder|ImageLibrary whereCreatedAt($value)
 * @method static Builder|ImageLibrary whereId($value)
 * @method static Builder|ImageLibrary whereTitle($value)
 * @method static Builder|ImageLibrary whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ImageLibrary extends Model implements HasMedia, Sortable
{
    use InteractsWithMedia;
    use SortableTrait;

    # https://docs.spatie.be/laravel-medialibrary/v7/converting-images/defining-conversions/#using-model-properties-in-a-conversion
    public $registerMediaConversionsUsingModelInstance = true;

    protected $fillable = [
        'title',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('image-library.tables.image_libraries'));
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile()
            ->registerMediaConversions(
                function (Media $media) {
                    foreach ($this->conversions as $conversion) {
                        $mediaConversion = $this->addMediaConversion($conversion->name);
                        if ($conversion->is_optimize) {
                            $mediaConversion->optimize();
                    }
                    if (!is_null($conversion->format)) {
                        $mediaConversion->format($conversion->format);
                    }

                    if (!is_null($conversion->fit) && !is_null($conversion->width) && !is_null($conversion->height)) {
                        $mediaConversion->fit($conversion->fit, $conversion->width, $conversion->height);
                    } else {
                        if (!is_null($conversion->width)) {
                            $mediaConversion->width($conversion->width);
                        }
                        if (!is_null($conversion->height)) {
                            $mediaConversion->height($conversion->height);
                        }
                    }

                }
            });
    }

    public function conversions()
    {
        return $this->belongsToMany(ImageConversion::class, config('image-library.tables.conversion_image_libraries'));
    }

}
