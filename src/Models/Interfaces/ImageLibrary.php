<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Models\Interfaces;

interface ImageLibrary
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function imageLibraries();

    /**
     * @param  string  $collectionName
     * @param  string  $conversionName
     *
     * @return string|null
     */
    public function getFirstImageUrl(string $collectionName, string $conversionName = ''): ?string;

    /**
     * @return int[]
     */
    public function getImageLibraryIds(): array;
}
