<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary;

use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\ImageLibraryCreateException;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class ImageLibrary
{
    /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository */
    private $imageLibraryRepository;

    /** @var string */
    private $redirectTo;

    /** @var array */
    private $imageLibraryCreateFields;

    public function __construct(ImageLibraryRepository $imageLibraryRepository)
    {
        $this->imageLibraryRepository = $imageLibraryRepository;
    }

    /**
     * @param  string  $to
     *
     * @return $this
     */
    public function redirect(string $to): self
    {
        $this->redirectTo = $to;

        return $this;
    }

    /**
     * @param  array  $fields
     *
     * @return $this
     * @throws \HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\ImageLibraryCreateException
     */
    public function imageLibraryCreateFields(array $fields): self
    {
        if (!array_key_exists('title', $fields)) {
            throw ImageLibraryCreateException::missingField('title');
        }

        $this->imageLibraryCreateFields = $fields;

        return $this;
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\Interfaces\ImageLibrary  $model
     * @param  array  $imageLibraryIDsOrFileUploads
     * @param  string  $categoryName
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upload(
        Models\Interfaces\ImageLibrary $model,
        array $imageLibraryIDsOrFileUploads,
        string $categoryName = 'default'
    ) {
        Validator::imageLibraryIDsOrFileUploaded($imageLibraryIDsOrFileUploads);

        DB::transaction(function () use (
            $imageLibraryIDsOrFileUploads,
            $model,
            $categoryName
        ) {

            array_map(function ($imageLibraryIDsOrFileUpload) use ($model, $categoryName) {

                if (is_int($imageLibraryIDsOrFileUpload)) {

                    $model->imageLibraries()->attach($imageLibraryIDsOrFileUpload,
                        ['category_name' => $categoryName]
                    );
                } else {

                    $this->uploadImage($model, $imageLibraryIDsOrFileUpload, $categoryName);
                }

            }, $imageLibraryIDsOrFileUploads);

        });

        RegenerateMedia::run($imageLibraryIDsOrFileUploads);

        return is_null($this->redirectTo) ? back() : redirect($this->redirectTo);
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\Interfaces\ImageLibrary  $model
     * @param  \Illuminate\Http\UploadedFile  $uploadedFile
     * @param  string  $categoryName
     *
     * @throws \HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\ImageLibraryCreateException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    private function uploadImage(
        Models\Interfaces\ImageLibrary $model,
        UploadedFile $uploadedFile,
        string $categoryName
    ) {
        if (blank($this->imageLibraryCreateFields)) {
            throw ImageLibraryCreateException::required();
        } elseif ($this->imageLibraryRepository->findWhere(['title' => $this->imageLibraryCreateFields['title']])->count() > 0) {
            throw ImageLibraryCreateException::duplicate('title', $this->imageLibraryCreateFields['title']);
        }

        /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary $imageLibrary */
        $imageLibrary = $this->imageLibraryRepository->create($this->imageLibraryCreateFields);

        $imageLibrary->addMedia($uploadedFile)
            ->toMediaCollection();

        $model->imageLibraries()->attach($imageLibrary->id,
            ['category_name' => $categoryName]
        );
    }


    /**
     * @param  array  $ids
     * @param  int  $startOrder
     */
    public function sort(array $ids, int $startOrder = 1)
    {
        Validator::imageLibraryIDsOrFileUploaded($ids, true);

        Models\ImageLibrary::setNewOrder($ids, $startOrder);
    }

    /**
     * @return \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository
     */
    public function repository(): ImageLibraryRepository
    {
        return app(ImageLibraryRepository::class);
    }

    /**
     * @return \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository
     */
    public function repositoryConversion(): ConversionRepository
    {
        return app(ConversionRepository::class);
    }
}
