<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Http\Controllers;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\RegenerateMedia;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ImageLibrariesController extends Controller
{
    private const VIEW_PATH = 'imageLibrary::';

    /**
     * @var \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository
     */
    private $imageLibraryRepository;
    /**
     * @var \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository
     */
    private $conversionRepository;

    /**
     * ImageLibrariesController constructor.
     *
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository  $imageLibraryRepository
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository  $conversionRepository
     */
    public function __construct(
        ImageLibraryRepository $imageLibraryRepository,
        ConversionRepository $conversionRepository
    ) {
        $this->imageLibraryRepository = $imageLibraryRepository;
        $this->conversionRepository = $conversionRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        MetaTag::setTags([
//            'title' => 'Image Library',
//        ]);
        $imageLibraries = $this->imageLibraryRepository
            ->orderBy('created_at', 'desc')
            ->paginate();
        return view(self::VIEW_PATH.'index', compact('imageLibraries'));
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary  $imageLibrary
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ImageLibrary $imageLibrary)
    {
//        MetaTag::setTags([
//            'title' => 'Show Image Library',
//        ]);
        return view(self::VIEW_PATH.'show', compact('imageLibrary'));
    }

    public function create()
    {
//        MetaTag::setTags([
//            'title' => 'Create Image Library',
//        ]);
        return view(self::VIEW_PATH.'create');
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $imageLibraryTableName = $this->imageLibraryRepository->makeModel()->getTable();
        $attributes = $this->validate($request, [
            'title' => "required|unique:$imageLibraryTableName",
//            'collections.*' => 'integer|in:'.$this->collectionRepository->all()->implode('id', ','),
            'image' => 'required|image',
        ]);

        $imageLibrary = DB::transaction(function () use ($attributes) {

            $imageLibrary = $this->imageLibraryRepository->create(Arr::only($attributes, 'title'));
            /** @var \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary $imageLibrary */
                    $imageLibrary
                        ->addMedia($attributes['image'])
                        ->toMediaCollection();

            flash('Image library created!', 'success');
            return $imageLibrary;
        });

        return redirect(route('image-libraries.edit', $imageLibrary));
    }

    public function edit(ImageLibrary $imageLibrary)
    {
//        MetaTag::setTags([
//            'title' => 'Edit Image Library',
//        ]);
        return view(self::VIEW_PATH.'edit', compact('imageLibrary'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary  $imageLibrary
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function update(Request $request, ImageLibrary $imageLibrary)
    {
        $imageLibraryTableName = $this->imageLibraryRepository->makeModel()->getTable();
        $conversionTableName = $this->conversionRepository->makeModel()->getTable();
        $attributes = $this->validate($request, [
            'image' => 'nullable|image',
            'title' => "required|string|unique:$imageLibraryTableName,title,{$imageLibrary->id}",
            'conversions.*' => "nullable|exists:$conversionTableName,id",
        ]);

        DB::transaction(function () use ($attributes, $imageLibrary) {

            /** @var ImageLibrary $imageLibrary */
            $imageLibrary = $this->imageLibraryRepository->update(Arr::only($attributes, 'title'), $imageLibrary->id);

            if (isset($attributes['conversions']) && !blank($attributes['conversions'])) {
                foreach ($this->conversionRepository->find($attributes['conversions']) as $conversion) {
                    $imageLibrary->conversions()->sync($conversion);
                }

                RegenerateMedia::run([$imageLibrary->id]);
            }

            if (isset($attributes['image']) && !blank($attributes['image'])) {
                $imageLibrary
                    ->addMedia($attributes['image'])
                    ->toMediaCollection();
            }

            flash('Image library updated', 'success');
        });

        return redirect(route('image-libraries.edit', $imageLibrary));
    }

}
