<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Http\Controllers;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use Illuminate\Http\Request;

//use MetaTag;

class ConversionsController extends Controller
{
    private const VIEW_PATH = 'imageLibrary::conversions.';

    /**
     * @var \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository
     */
    private $conversionRepository;
    /**
     * @var \HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository
     */
    private $imageLibraryRepository;

    public function __construct(
        ConversionRepository $conversionRepository,
        ImageLibraryRepository $imageLibraryRepository
    )
    {
        $this->conversionRepository = $conversionRepository;
        $this->imageLibraryRepository = $imageLibraryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        MetaTag::setTags([
//            'title' => 'Conversion Image Library',
//        ]);
        $conversions = $this->conversionRepository->paginate();
        return view(self::VIEW_PATH.'index', compact('conversions'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $conversionTableName = $this->conversionRepository->makeModel()->getTable();

        $attributes = $this->validate($request, [
            'name' => "required|unique:$conversionTableName",
            'fit' => 'nullable|in:'.implode(',', array_keys(config('image-library.fit'))),
            'format' => 'nullable|in:'.implode(',', array_keys(config('image-library.format'))),
            'width' => 'nullable|integer|min:1',
            'height' => 'nullable|integer|min:1',
            'is_optimize' => 'nullable|bool',
        ]);

        $attributes['is_optimize'] = isset($attributes['is_optimize']);

        $conversion = $this->conversionRepository->create($attributes);

        flash('Conversion created!', 'success');

        return redirect(route('image-libraries.conversions.edit', $conversion));
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion  $conversion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ImageConversion $conversion)
    {
//        MetaTag::setTags([
//            'title' => 'Edit Conversion',
//        ]);

        return view(self::VIEW_PATH.'edit', compact('conversion'));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion  $conversion
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, ImageConversion $conversion)
    {
        $conversionTableName = $this->conversionRepository->makeModel()->getTable();

        $attributes = $this->validate($request, [
            'name' => "required|string|unique:$conversionTableName,name,{$conversion->id}",
            'fit' => 'nullable|in:'.implode(',', array_keys(config('image-library.fit'))),
            'format' => 'nullable|in:'.implode(',', array_keys(config('image-library.format'))),
            'width' => 'nullable|integer|min:1',
            'height' => 'nullable|integer|min:1',
            'is_optimize' => 'nullable|bool',
        ]);

        $attributes['is_optimize'] = isset($attributes['is_optimize']);

        $conversion = $this->conversionRepository->update($attributes, $conversion->id);

        flash('Conversion updated!', 'success');

        return redirect(route('image-libraries.conversions.edit', $conversion));
    }
}
