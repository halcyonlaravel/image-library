<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion;
use Prettus\Repository\Eloquent\BaseRepository;

class ConversionRepositoryEloquent extends BaseRepository implements ConversionRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ImageConversion::class;
    }
}
