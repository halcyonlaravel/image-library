<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use Prettus\Repository\Eloquent\BaseRepository;

class ImageLibraryRepositoryEloquent extends BaseRepository implements ImageLibraryRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ImageLibrary::class;
    }
}
