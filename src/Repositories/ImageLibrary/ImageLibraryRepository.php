<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ConversionRepository
 *
 * @package HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion
 * @package App\Repositories
 * @method BaseRepository pushCriteria(CriteriaInterface $param)
 * @method Model makeModel()
 * @method model()
 */
interface ImageLibraryRepository extends RepositoryInterface
{

}
