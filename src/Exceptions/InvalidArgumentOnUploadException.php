<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Exceptions;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use Illuminate\Http\UploadedFile;

class InvalidArgumentOnUploadException extends BaseException
{
    public static function create()
    {
        return new static('Argument values must be ID exist in '.ImageLibrary::class.' or instance of '.UploadedFile::class);
    }
}