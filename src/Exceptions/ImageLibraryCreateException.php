<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Exceptions;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;

class ImageLibraryCreateException extends BaseException
{
    public static function missingField(string $field)
    {
        return new static("Missing $field field");
    }

    public static function duplicate(string $field, string $value)
    {
        return new static("$field [$value] already exist on ".ImageLibrary::class);
    }

    public static function required()
    {
        return new static('Missing required fields please use ->imageLibraryCreateFields(array $fields)');
    }
}