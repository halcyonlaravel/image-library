<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Exceptions;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;

class InvalidArgumentOnSortingException extends BaseException
{
    public static function create()
    {
        return new static('Invalid Argument value in sort, must be ID exist in '.ImageLibrary::class);
    }
}