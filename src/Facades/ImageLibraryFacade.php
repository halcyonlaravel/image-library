<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Facades;

use HalcyonLaravelBoilerplate\ImageLibrary\ImageLibrary as IL;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\Interfaces\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Facade;

/**
 * @method static RedirectResponse upload(ImageLibrary $model, array $imageLibraryIDs, string $categoryName = 'default')
 * @method static IL redirect(string $to)
 * @method static IL imageLibraryCreateFields(array $fields)
 * @method static ImageLibraryRepository repository()
 * @method static ConversionRepository repositoryConversion()
 */
class ImageLibraryFacade extends Facade
{
    public static function getFacadeAccessor()
    {
        return IL::class;
    }
}
