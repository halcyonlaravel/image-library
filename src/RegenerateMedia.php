<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary;

use Illuminate\Support\Facades\Artisan;

class RegenerateMedia
{
    /**
     * @param  array  $imageLibraryIDsOrFileUploads
     */
    public static function run(array $imageLibraryIDsOrFileUploads)
    {
        $ids = [];

        foreach ($imageLibraryIDsOrFileUploads as $imageLibraryIDsOrFileUpload) {
            if (is_int($imageLibraryIDsOrFileUpload)) {
                $ids[] = $imageLibraryIDsOrFileUpload;
            }
        }

        Artisan::call(
            'media-library:regenerate',
            [
                '--ids' => implode(',', $ids),
            ]
        );
    }
}