<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Support\TestModels;

use HalcyonLaravelBoilerplate\ImageLibrary\Models\Interfaces\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\Traits\HasImageLibraryTrait;
use Illuminate\Database\Eloquent\Model;

class TestModel extends Model implements ImageLibrary
{
    use HasImageLibraryTrait;

    public $timestamps = false;
    protected $table = 'test_models';
    protected $guarded = [];

}