<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use PDO;

trait Temporary
{
    public function loadBluePrintMacros()
    {
        $foreignConstraint = function (
            bool $isRelationBigInteger,
            Blueprint $blueprint,
            $column,
            string $foreignTableName,
            string $onDelete = null,
            string $name = null
        ) {
            $tbl = $blueprint
                ->{($isRelationBigInteger ? 'bigInteger' : 'integer')}($column)
                ->unsigned();

            if (is_null($onDelete)) {

                $blueprint->foreign($column, $name)
                    ->references('id')
                    ->on($foreignTableName);
            } else {

                $blueprint->foreign($column)
                    ->references('id')
                    ->on($foreignTableName)
                    ->onDelete($onDelete);
            }
            return $tbl;
        };
        Blueprint::macro('foreignConstraintBigInteger', function (
            $column,
            string $foreignTableName,
            string $onDelete = null,
            string $name = null
        ) use ($foreignConstraint) {
            return $foreignConstraint(true, $this, $column, $foreignTableName, $onDelete, $name);
        });
        Blueprint::macro('foreignConstraint', function (
            $column,
            string $foreignTableName,
            string $onDelete = null,
            string $name = null
        ) use ($foreignConstraint) {
            return $foreignConstraint(false, $this, $column, $foreignTableName, $onDelete, $name);
        });

        $isLatestMysqlVersion = $this->isLatestMysqlVersion();
        Blueprint::macro('jsonable', function ($column) use ($isLatestMysqlVersion) {
            $type = $isLatestMysqlVersion ? 'json' : 'longText';
            return $this->$type($column);
        });
    }

    private function isLatestMysqlVersion(): bool
    {
        $pdo = DB::connection()->getPdo();
        return ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql') &&
            version_compare($pdo->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge');
    }
}