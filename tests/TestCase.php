<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests;

use CreateImageLibraryTables;
use CreateMediaTable;
use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\Providers\ImageLibraryServiceProvider;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\Support\TestModels\TestModel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Orchestra\Testbench\TestCase as Orchestra;
use Spatie\MediaLibrary\MediaLibraryServiceProvider;
use Spatie\MediaLibrary\Support\PathGenerator\DefaultPathGenerator;

abstract class TestCase extends Orchestra
{
    use Temporary;

    /** @var string */
    const TEST_FILE_NAME = 'testFile.jpg';

    /** @var TestModel */
    protected $testModel;

    /** @var ImageLibrary */
    protected $testImageLibraryModel;

    /** @var ImageConversion */
    protected $testImageConversionModel;

    /**
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadBluePrintMacros();

        $this->setUpDatabase($this->app);

        Storage::fake('public');

        $this->testModel = TestModel::first();
        $this->testImageLibraryModel = ImageLibrary::first();
        $this->testImageConversionModel = ImageConversion::first();

        $this->testImageLibraryModel
            ->copyMedia(UploadedFile::fake()->image(self::TEST_FILE_NAME))
            ->toMediaCollection();
    }

    protected function setUpDatabase($app)
    {
        $app['db']->connection()->getSchemaBuilder()->create('test_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        include_once __DIR__.'/../database/migrations/create_image_library_tables.php.stub';
        (new CreateImageLibraryTables())->up();

        include_once __DIR__.'/../vendor/spatie/laravel-medialibrary/database/migrations/create_media_table.php.stub';
        (new CreateMediaTable())->up();

        TestModel::create(['name' => 'test']);
        ImageLibrary::create(['title' => 'test']);
        ImageConversion::create(['name' => 'test']);
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary  $imageLibrary
     * @param  string|null  $fileName
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion|null  $imageConversion
     * @param  string  $disk
     */
    protected function assertMediaFileExist(
        ImageLibrary $imageLibrary,
        string $fileName = null,
        ImageConversion $imageConversion = null,
        string $disk = 'public'
    ): void {

        Storage::disk($disk)->assertExists($this->getMediaPath($imageLibrary, ($fileName ?? self::TEST_FILE_NAME)));
    }

    /**
     * @param  \HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary  $imageLibrary
     * @param  string|null  $fileName
     *
     * @return string
     */
    protected function getMediaPath(ImageLibrary $imageLibrary, string $fileName = null)
    {
        return app(DefaultPathGenerator::class)
//                ->create()
                ->getPath($imageLibrary->getFirstMedia()).'/'.($fileName ?: self::TEST_FILE_NAME);
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }

    protected function getPackageAliases($app)
    {
        return [
            'ImageLibrary' => ImageLibraryFacade::class,
        ];
    }

    protected function getPackageProviders($app)
    {
        return [
            ImageLibraryServiceProvider::class,
            MediaLibraryServiceProvider::class,
        ];
    }
}
