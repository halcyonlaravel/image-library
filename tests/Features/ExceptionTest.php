<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\ImageLibraryCreateException;
use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\InvalidArgumentOnUploadException;
use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;
use Illuminate\Http\UploadedFile;

class ExceptionTest extends TestCase
{


    /**
     * @test
     */
    public function invalid_id()
    {
        $this->expectException(InvalidArgumentOnUploadException::class);
        $this->expectExceptionMessage('Argument values must be ID exist in '.ImageLibrary::class.' or instance of '.UploadedFile::class);

        ImageLibraryFacade::upload($this->testModel, [9]);
    }

    /**
     * @test
     */
    public function failed_on_unique_title()
    {
        $this->expectException(ImageLibraryCreateException::class);
        $this->expectExceptionMessage('title [test] already exist on '.ImageLibrary::class);

        ImageLibraryFacade::imageLibraryCreateFields([
            'title' => 'test',
        ])->upload($this->testModel, [
            UploadedFile::fake()->image(self::TEST_FILE_NAME),
        ]);
    }

    /**
     * @test
     */
    public function required_all_fields()
    {
        $this->expectException(ImageLibraryCreateException::class);
        $this->expectExceptionMessage('Missing required fields please use ->imageLibraryCreateFields(array $fields)');

        ImageLibraryFacade::upload($this->testModel, [
            UploadedFile::fake()->image(self::TEST_FILE_NAME),
        ]);
    }

    /**
     * @test
     */
    public function required_title_fields()
    {
        $this->expectException(ImageLibraryCreateException::class);
        $this->expectExceptionMessage('Missing title field');

        ImageLibraryFacade::imageLibraryCreateFields([
            'xxxx' => 'xxxx',
        ])->upload($this->testModel, [
            UploadedFile::fake()->image(self::TEST_FILE_NAME),
        ]);
    }

    /**
     * @test
     */
    public function failed_on_upload_mixed()
    {
        $this->expectException(InvalidArgumentOnUploadException::class);
        $this->expectExceptionMessage('Argument values must be ID exist in '.ImageLibrary::class.' or instance of '.UploadedFile::class);

        ImageLibraryFacade::upload($this->testModel, [
            UploadedFile::fake()->image(self::TEST_FILE_NAME),
            123, // not exist id
        ]);
    }
}