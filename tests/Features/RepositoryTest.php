<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\Conversion\ConversionRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Repositories\ImageLibrary\ImageLibraryRepository;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;

class RepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function repository()
    {
        $this->assertInstanceOf(ImageLibraryRepository::class, ImageLibraryFacade::repository());
    }

    /**
     * @test
     */
    public function repositoryConversion()
    {
        $this->assertInstanceOf(ConversionRepository::class, ImageLibraryFacade::repositoryConversion());
    }
}