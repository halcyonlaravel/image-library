<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;
use Illuminate\Http\RedirectResponse;

class RedirectTest extends TestCase
{
    /**
     * @test
     */
    public function default_redirect()
    {
        $return = ImageLibraryFacade::upload($this->testModel, [$this->testImageLibraryModel->id]);

        $this->assertInstanceOf(RedirectResponse::class, $return);
    }

    /**
     * @test
     */
    public function custom_redirect()
    {
        $return = ImageLibraryFacade::redirect('http://test.web')
            ->upload($this->testModel, [$this->testImageLibraryModel->id]);

        $this->assertInstanceOf(RedirectResponse::class, $return);
    }
}