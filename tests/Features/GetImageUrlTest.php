<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;

class GetImageUrlTest extends TestCase
{
    /**
     * @test
     */
    public function get_image_url()
    {
        ImageLibraryFacade::upload($this->testModel, [$this->testImageLibraryModel->id]);

        $imgUrl = $this->testModel->refresh()->getFirstImageUrl('default');
        $this->assertEquals('/storage/1/testFile.jpg', $imgUrl);
    }
}