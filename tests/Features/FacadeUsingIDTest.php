<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageConversion;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;
use Spatie\Image\Manipulations;

class FacadeUsingIDTest extends TestCase
{
    /**
     * @test
     */
    public function success_on_upload()
    {
        ImageLibraryFacade::upload($this->testModel, [$this->testImageLibraryModel->id]);

        $this->assertMediaFileExist($this->testImageLibraryModel);
    }

    /**
     * @test
     */
    public function success_on_upload_with_conversion()
    {
        $this->testImageLibraryModel->conversions()->save(ImageConversion::create(
            [
                'name' => 'test',
                'format' => Manipulations::FORMAT_JPG,
//                'fit' => Manipulations::FIT_CROP,
                'width' => 10,
                'height' => 10,
            ]));
        $this->testImageLibraryModel->conversions()->save(ImageConversion::create(
            [
                'name' => 'test2',
                'format' => Manipulations::FORMAT_JPG,
                'fit' => Manipulations::FIT_CROP,
                'width' => 10,
                'height' => 10,
            ]));

        ImageLibraryFacade::upload($this->testModel, [$this->testImageLibraryModel->id]);
        $this->assertMediaFileExist($this->testImageLibraryModel);

        $this->assertTrue(true);
    }

}