<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Exceptions\InvalidArgumentOnSortingException;
use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Models\ImageLibrary;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;
use Illuminate\Http\UploadedFile;

class SortTest extends TestCase
{
    /**
     * @test
     */
    public function sorting()
    {
        ImageLibraryFacade::sort([1]);

        // no need to test this,
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function invalid_id()
    {
        $this->expectException(InvalidArgumentOnSortingException::class);
        $this->expectExceptionMessage('Invalid Argument value in sort, must be ID exist in '.ImageLibrary::class);

        ImageLibraryFacade::sort([99]);
    }

    /**
     * @test
     */
    public function file_not_allowed()
    {
        $this->expectException(InvalidArgumentOnSortingException::class);
        $this->expectExceptionMessage('Invalid Argument value in sort, must be ID exist in '.ImageLibrary::class);

        ImageLibraryFacade::sort([UploadedFile::fake()->image(self::TEST_FILE_NAME)]);
    }
}