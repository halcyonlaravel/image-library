<?php

namespace HalcyonLaravelBoilerplate\ImageLibrary\Tests\Features;

use HalcyonLaravelBoilerplate\ImageLibrary\Facades\ImageLibraryFacade;
use HalcyonLaravelBoilerplate\ImageLibrary\Tests\TestCase;
use Illuminate\Http\UploadedFile;

class FacadeUsingUploadedFileTest extends TestCase
{
    /**
     * @test
     */
    public function success_on_upload()
    {
        $this->assertCount(0, $this->testModel->imageLibraries);

        ImageLibraryFacade::imageLibraryCreateFields([
            'title' => 'test1',
        ])->upload($this->testModel, [UploadedFile::fake()->image(self::TEST_FILE_NAME)]);

        $this->assertCount(1, $this->testModel->refresh()->imageLibraries);
        $this->assertCount(1, $this->testModel->refresh()->imageLibraries->first()->media);
    }

    /**
     * @test
     */
    public function success_on_upload_mixed()
    {
        $this->assertCount(0, $this->testModel->imageLibraries);

        ImageLibraryFacade::imageLibraryCreateFields([
            'title' => 'test1',
        ])->upload($this->testModel, [
            UploadedFile::fake()->image(self::TEST_FILE_NAME),
            $this->testImageLibraryModel->id,
        ]);

        $this->assertCount(2, $this->testModel->refresh()->imageLibraries);
    }

}